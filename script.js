/* Экранирование - это способ правильно воспринимать интерпретатор JS спецсимволы в строке, например
let str = 'Hello \nWorld!';
 */


function createNewUser() {

    let firstName = prompt('What is your name?');
    let lastName = prompt('What is your last name?');
    let birthday = prompt('When were you born?', 'dd.mm.yyyy');

    let arr = birthday.split('.');
    let dateBirthday = new Date(`${arr[2]}, ${arr[1]}, ${arr[0]}`);
    let userAge = Math.floor((Date.now() - dateBirthday) / 31556952000);

    function User(name, lastName) {
        this.name = name;
        this.lastName = lastName;
        this.getLogin = function() {
            return `Your login: ${this.name.charAt(0).toLowerCase() + this.lastName.toLowerCase()}`;
        };
        this.getAge = function () {
            return `User age: ${userAge}`;
        };
        this.getPassword = function () {
            return `Your password: ${this.name.charAt(0).toUpperCase() + this.lastName.toLowerCase() + arr[2]}`;
        }
    }
    return new User(firstName, lastName);
}

let artem = createNewUser();
console.log(artem.getLogin());
console.log(artem.getAge());
console.log(artem.getPassword());